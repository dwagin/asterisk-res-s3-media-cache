;
; Sample configuration for res_s3_media_cache
;
; res_s3_media_cache is the S3 backend for the core media cache. The
; following options can be used to tune the behavior of the implementation
; or left as default.
;
; See the module's and cURL's documentation for the exact meaning of these
; options.


[general]
; Maximum time in seconds the transfer is allowed to complete in.
;
; See https://curl.se/libcurl/c/CURLOPT_TIMEOUT.html for details.
;
;timeout_secs = 180


; The HTTP User-Agent to use for requests.
;
; See https://curl.se/libcurl/c/CURLOPT_USERAGENT.html for details.
;
;user_agent = asterisk-libcurl-agent/1.0


; Follow HTTP 3xx redirects on requests. This can be combined with the
; max_redirects option to limit the number of times a redirect will be
; followed per request.
;
; See https://curl.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html for details.
;
;follow_location =  false


; The maximum number of redirects to follow.
;
; See https://curl.se/libcurl/c/CURLOPT_MAXREDIRS.html for details.
;
;max_redirects = 8

; The HTTP/HTTPS proxy to use for requests. Leave unspecified to not use
; a proxy. This can be a URL with scheme, host and port.
;
; See https://curl.se/libcurl/c/CURLOPT_PROXY.html for details.
;
;proxy = https://localhost:1234


; The life-time for DNS cache entries.
;
; See https://curl.se/libcurl/c/CURLOPT_DNS_CACHE_TIMEOUT.html for details.
;
;dns_cache_timeout_secs = 60
